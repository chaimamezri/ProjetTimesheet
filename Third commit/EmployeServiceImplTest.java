package tn.esprit.spring;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.services.IEmployeService;

@RunWith(SpringRunner.class)
@SpringBootTest

public class EmployeServiceImplTest {
	
	@Autowired 
	IEmployeService employeService;
	
	@Mock
	//@Autowired
	EmployeRepository employeRepository;
	
	
	
	@Test
	public void testAddOrUpdateEmploye()  {
		
		Employe e = new Employe("jamie", "Kent", "jamie.ken@gmail.com","kent",true, Role.INGENIEUR);
		
		int employeAdded =  employeService.addOrUpdateEmploye(e); 
		
	       assertEquals(e.getId(), employeAdded);
	      		       
	}
	
	@Test
	public void testGetEmployePrenomById() {
	
		
//		Employe e = new Employe();
//		
//		e.setId(20);
//		e.setEmail("aa@bb.com");
//		e.setPassword("aaa");
//		e.setNom("Poly");
//		e.setPrenom("crow");
//		e.setActif(true);
//		e.setRole(Role.INGENIEUR);
		
		
		//String employePrenom = employeService.getEmployePrenomById(1); 
		
	 when  (employeRepository.findById((20))).thenReturn(null);
			 
		//assertEquals(e.getPrenom(), employePrenom);
		
	
	}
	
	
	

	@Test
	public void testGetAllEmployes() {
		
		List<Employe> listEmployes = employeService.getAllEmployes(); 
		// if there are 5 users in DB : 
		assertEquals(11, listEmployes.size());
	}
	
	
//	@Test
//	public void testDeleteEmployeById() {
//		
//		
//	
//		Employe employe = employeRepository.findById(1).get();
//		
//	        employeRepository.deleteById(1);
//
//		assertEquals(1L, employe.getId().longValue());
//	}
//	
	
	
	/*@Test
	public void testMettreAjourEmailByEmployeId(String email, int employeId) {
		Employe employe = employeRepository.findById(2).get();
		employe.setEmail("jamie.kant@gmail.com");

		when(employeRepository.findBy((20))).thenReturn(null);
		// employeRepository.save(employe);	
}*/

	@Test
	public void testVerifyBeans(){
		Employe employe = new Employe();
		employe.verifyBeans();
	}
	

}
