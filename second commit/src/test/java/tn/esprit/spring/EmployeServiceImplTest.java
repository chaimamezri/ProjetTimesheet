package tn.esprit.spring;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Role;

import tn.esprit.spring.services.IEmployeService;

@RunWith(SpringRunner.class)
@SpringBootTest

public class EmployeServiceImplTest {
	
	@Autowired 
	IEmployeService employeService;
	
	
	
	@Test
	public void testAddOrUpdateEmploye()  {
		
		Employe e = new Employe("jamie", "Kent", "jamie.ken@gmail.com","kent",true, Role.INGENIEUR);
		
		int employeAdded =  employeService.addOrUpdateEmploye(e); 
		
	       assertEquals(e.getId(), employeAdded);
	      		       
	}
	
/*	@Test
	public void testGetEmployePrenomById() {
	
		
		Employe e = new Employe("pollyanna", "crow", "pollyanna.crow@gmail.com","kent",false, Role.INGENIEUR);
		
		String employePrenom = employeService.getEmployePrenomById(1); 
	 
		assertEquals(e.getPrenom(), employePrenom);
	}*/
	
	
	@Test
	public void testGetAllEmployes() {
		
		List<Employe> listEmployes = employeService.getAllEmployes(); 
		// if there are 5 users in DB : 
		assertEquals(8, listEmployes.size());
	}
	
	
//	@Test
//	public void testDeleteEmployeById() {
//		
//		Employe employeRetrieved =employeService.deleteEmployeById;
//		
//		assertEquals(1L, employeRetrieved.getId().longValue());
//	}
	
	@Test
	public void testVerifyBeans(){
		Employe employe = new Employe();
		employe.verifyBeans();
	}
	

}
