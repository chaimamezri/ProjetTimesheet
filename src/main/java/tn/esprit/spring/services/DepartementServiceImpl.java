package tn.esprit.spring.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.repository.ContratRepository;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.TimesheetRepository;
import org.apache.log4j.Logger;


@Service
public class DepartementServiceImpl implements IDepartementService {
	@Autowired
	EmployeRepository employeRepository;
	@Autowired
	DepartementRepository deptRepoistory;
	@Autowired
	ContratRepository contratRepoistory;
	@Autowired
	TimesheetRepository timesheetRepository;

	private static final Logger l = Logger.getLogger(EntrepriseServiceImpl.class);
	
	@Transactional
	public int  deleteDepartementById(int depId) {
		
		l.debug("methode deleteDepartementById running ");
		
		try {
			deptRepoistory.delete(deptRepoistory.findById(depId).get());
			l.info("departement  deleted");
			return 0;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			l.error("error  deleteDepartementById " +e);
			return -1;
		}
		
	
	
	}
		
	public List<Departement> getAllDepartements() {
		l.debug("methode getAllDepartements running ");
		List<Departement> list=new ArrayList<>();
		
		try {
			list= (List<Departement>) deptRepoistory.findAll();
			
		} catch (Exception e) {
			l.error("error getAllDepartements " +e);
			// TODO Auto-generated catch block
			
		}
		return list;
		
	}
	public int ajouterDepartement(Departement dep) {
		l.debug("methode ajouterDepartement running ");
		try {
			deptRepoistory.save(dep);
			return dep.getId();
		} catch (Exception e) {
			l.error("error  ajouterDepartement " +e);
			return -1;
			
		}
	}

}
