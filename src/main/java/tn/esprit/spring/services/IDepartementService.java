package tn.esprit.spring.services;

import java.util.List;

import tn.esprit.spring.entities.Departement;


public interface IDepartementService {
	
	
	public List<Departement> getAllDepartements();
	public int deleteDepartementById(int depId);
	public int ajouterDepartement(Departement dep);



	
	
	

	
}
