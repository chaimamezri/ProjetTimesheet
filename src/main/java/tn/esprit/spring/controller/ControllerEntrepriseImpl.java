package tn.esprit.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.services.IEmployeService;
import tn.esprit.spring.services.IEntrepriseService;
import tn.esprit.spring.services.ITimesheetService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Controller
public class ControllerEntrepriseImpl{
	
	private static final Logger l = LogManager.getLogger(ControllerEntrepriseImpl.class);

	@Autowired
	IEmployeService iemployeservice;
	@Autowired
	IEntrepriseService ientrepriseservice;
	@Autowired
	ITimesheetService itimesheetservice;
	
	

	public int ajouterEntreprise(Entreprise ssiiConsulting) {
		ientrepriseservice.ajouterEntreprise(ssiiConsulting);
		
		l.info("Entreprise added!");
		
		return ssiiConsulting.getId();
		
		
	}
	public void affecterDepartementAEntreprise(int depId, int entrepriseId) {
		l.info("In displayEntreprise : " ); 
		ientrepriseservice.affecterDepartementAEntreprise(depId, entrepriseId);
		l.info("Out  displayEntreprise : "  ); 
	}
	public void deleteEntrepriseById(int entrepriseId)
	
	{	l.debug("Remove entreprise!" + entrepriseId  );
		ientrepriseservice.deleteEntrepriseById(entrepriseId);

		l.info("Entreprise Removed!" + entrepriseId  );
	}
	public Entreprise getEntrepriseById(int entrepriseId) {
		l.debug("Get entreprise" + entrepriseId  );
		l.info("Entreprise shown!" + entrepriseId  );

		return ientrepriseservice.getEntrepriseById(1);
		
	}
	
	public int ajouterDepartement(Departement dep) {
		l.info("department added!" + dep  );
		return ientrepriseservice.ajouterDepartement(dep);
	}
	
	public List<String> getAllDepartementsNamesByEntreprise(int entrepriseId) {
		return ientrepriseservice.getAllDepartementsNamesByEntreprise(entrepriseId);
	}

	public void deleteDepartementById(int depId) {
		l.debug("remove department" + depId  );
		
		ientrepriseservice.deleteDepartementById(depId);
		l.info("department removed" + depId  );
		
	}
}
