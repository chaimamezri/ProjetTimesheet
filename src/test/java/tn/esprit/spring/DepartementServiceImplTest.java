package tn.esprit.spring;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.services.IDepartementService;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DepartementServiceImplTest {
	@Autowired
	IDepartementService ds;
	
	int id=3;
	@Test
	public void testAjouterDepartement() {
		int id= ds.ajouterDepartement(new Departement("RH"));
		
		assertNotEquals(-1, id);
		
	}
	
	
	@Test
	public void testDeleteDepartementById() {
		int i= ds.deleteDepartementById(id);
		
		assertNotEquals(-1, i);
		
	}
	
	@Test
	public void testGetAllDepartements() {
		List<Departement> i= ds.getAllDepartements();
		
		assertNotNull(i);
		
	}
	

}
