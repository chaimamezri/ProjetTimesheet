package tn.esprit.spring;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;



import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EntrepriseRepository;
import tn.esprit.spring.services.IEntrepriseService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class EntrepriseServiceImplTest {

	@Autowired
    EntrepriseRepository entrepriseRepoistory;
	@Autowired
	DepartementRepository deptRepoistory;
	
	@Autowired 
	IEntrepriseService entrepriseService;
	
	@Test
	public void testAjouterEntreprise() {
		Entreprise e = new Entreprise("DTL", "raisonSocial");
		 entrepriseService.ajouterEntreprise(e); 
		 assertEquals(e.getName(), "DTL");
	}
	@Test
	public void testGetEntrepriseById() {
	
		Entreprise e = new Entreprise("The haribees", "raisonSocial");	
		entrepriseService.ajouterEntreprise(e); 
		Entreprise ent = entrepriseService.getEntrepriseById(1); 
		
	
			 
		assertEquals(e.getName(), ent);
		
	
	}
	@Test
	public void testdeleteEntrepriseById() {
		
		
		Entreprise e = new Entreprise("DTL", "raisonSocial");	
		
		entrepriseService.ajouterEntreprise(e); 
		
		entrepriseRepoistory.deleteById(8);

	    boolean exists = entrepriseRepoistory.existsById(8);
		assertEquals(false, exists);
}}
